import pytest
import requests
from bs4 import BeautifulSoup

URL = 'https://the-internet.herokuapp.com/context_menu'
STR_1 = "Right-click in the box below to see one called 'the-internet'"
STR_2 = "Alibaba"


def get_text():
    res = requests.get(URL)
    html_page = res.content
    soup = BeautifulSoup(html_page, 'html.parser')
    return soup.find_all(text=True)


def check_string_exists(string_to_search):
    text = get_text()
    for i in text:
        if string_to_search in i:
            return True
    return False


def test1():
    assert check_string_exists(STR_1), f"String {STR_1} is not in page."


def test2():
    assert check_string_exists(STR_2), f"String {STR_2} is not in page."


def run_all_tests():
    test1()
    test2()


